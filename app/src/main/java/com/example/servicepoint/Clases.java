package com.example.servicepoint;

public class Clases {
    String claseid, nombre;
    public Clases(String claseid, String nombre) {
        this.claseid = claseid;
        this.nombre = nombre;
    }

    public String getClaseid() {
        return claseid;
    }

    public String getNombre() {
        return nombre;
    }
}
