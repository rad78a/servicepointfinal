package com.example.servicepoint.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.servicepoint.DetalleTrabajo;
import com.example.servicepoint.MainActivity;
import com.example.servicepoint.R;
import com.example.servicepoint.ui.gallery.GalleryFragment;

public class HomeFragment extends Fragment {
    Button botonenviar;
    ListView listaa;
    String[][] datos = {
            {"Trabajos de Pintura", "Pintado de casas", "Manta", "9", "Los Pintores preparan, protegen y decoran superficies interiores y exteriores, tales como madera, albañilería, mampostería, áreas de yeso o blanquecinas, cemento, estuco y metal.[1] Para ello, hacen uso de pinturas, papel tapiz y demás tipos de acabados (revestimientos variados de alta calidad, materiales a prueba de agua, materiales ignífugos, barnices, lacas, entre otros)."},
            {"Trabajos de Carpintería", "Trabajos en todo tipo de madera", "Manta", "7", "La carpintería es el nombre del oficio y del taller o lugar en donde se trabajan tanto la madera como sus derivados, y a quien lo ejerce se le denomina carpintero. Su objetivo es cambiar la forma física de la materia prima para crear objetos útiles al desarrollo humano, como pueden ser muebles para el hogar, marcos para puertas, molduras, juguetes, escritorios, librerías y otros."},
            {"Podado", "Todo tipo de planta", "Manta", "8", "Podar es el proceso de recortar un árbol o arbusto. Hecho con cuidado y correctamente, la poda puede incrementar el rendimiento del fruto; así, es una práctica agrícola común. En producción forestal se emplea para obtener fustes más rectos y con menos ramificaciones, por tanto de mayor calidad."},
            {"Trabajos de Electricidad", "Todo tipo de trabajos electricos", "Manta", "7", "Podar es el proceso de recortar un árbol o arbusto. Hecho con cuidado y correctamente, la poda puede incrementar el rendimiento del fruto; así, es una práctica agrícola común. En producción forestal se emplea para obtener fustes más rectos y con menos ramificaciones, por tanto de mayor calidad."},
            {"Trabajos de Plomeria", "Todo en plomeria", "Manta", "9", "Técnico en trabajos de instalación, mantenimiento, reparación, etc., de las conducciones de agua y aparatos sanitarios de una vivienda o edificio."},
            {"Trabajos de Soldadura", "Todo tipo de soldado", "Manta", "8", "Los soldadores se dedican básicamente a unir piezas mediante la aplicación de calor intenso con el objeto de obtener un cuerpo resultante que sea homogéneo y rígido. A este trabajo se le denomina soldadura y normalmente se realiza entre metales, aunque también se trabajan materiales termoplásticos"}
    };

    int[] datosImg = {R.drawable.interestelar, R.drawable.logan, R.drawable.everest, R.drawable.titanes, R.drawable.exmachina, R.drawable.arrival};



    private HomeViewModel homeViewModel;
        public View onCreateView (@NonNull LayoutInflater inflater,
                ViewGroup container, Bundle savedInstanceState){

            homeViewModel =
                    ViewModelProviders.of(this).get(HomeViewModel.class);
            View root = inflater.inflate(R.layout.fragment_home, container, false);
            final TextView textView = root.findViewById(R.id.text_home);
            final ListView listaa = root. findViewById(R.id.lista);
            listaa.setAdapter(new Adaptador(getActivity(), datos, datosImg));

            listaa.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent visorDetalles = new Intent(view.getContext(), DetalleTrabajo.class);
                    visorDetalles.putExtra("TIT", datos[position][0]);
                    visorDetalles.putExtra("DET", datos[position][4]);
                    startActivity(visorDetalles);
                }
            });
            homeViewModel.getText().observe(this, new Observer<String>() {

                @Override
                public void onChanged(@Nullable String s) {
                    textView.setText(s);
                }
            });


            return root;



        }

}
