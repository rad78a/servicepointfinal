package com.example.servicepoint;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class registro extends AppCompatActivity implements View.OnClickListener {


    //defining view objects
    private EditText TextEmail, TextNombre;
    private EditText TextPassword;
    private Button btnRegistrar, btnConfi;
    private ProgressDialog progressDialog;
    private DatabaseReference Clases;
// ...


    //Declaramos un objeto firebaseAuth
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        //inicializamos el objeto firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();
        Clases = FirebaseDatabase.getInstance().getReference("Clases");

        //Referenciamos los views
        TextNombre = (EditText) findViewById(R.id.txtNombr);
        TextEmail = (EditText) findViewById(R.id.TxtEmail);
        TextPassword = (EditText) findViewById(R.id.TxtPassword);


        btnRegistrar = (Button) findViewById(R.id.botonRegistrar);
        btnConfi = (Button) findViewById(R.id.botonConf);

        progressDialog = new ProgressDialog(this);

        //attaching listener to button
        btnRegistrar.setOnClickListener(registro.this);
        btnConfi.setOnClickListener(registro.this);
    }

    private void registrarUsuario() {

        //Obtenemos el email y la contraseña desde las cajas de texto
        String email = TextEmail.getText().toString().trim();
        String password = TextPassword.getText().toString().trim();


        //Verificamos que las cajas de texto no esten vacías
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Se debe ingresar un email", Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Falta ingresar la contraseña", Toast.LENGTH_LONG).show();
            return;
        }


        progressDialog.setMessage("Realizando registro en linea...");
        progressDialog.show();

        //creating a new user
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if (task.isSuccessful()) {

                            Toast.makeText(registro.this, "Se ha registrado el usuario con el email: " + TextEmail.getText(), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(registro.this, MainActivity.class);
                            startActivity(intent);
                        } else {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {

                                Toast.makeText(registro.this, "Ese usuario ya existe ", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(registro.this, "No se pudo registrar el usuario ", Toast.LENGTH_LONG).show();
                            }
                        }
                        progressDialog.dismiss();
                    }
                });

    }

    public void registrarClase() {
        String nombre = TextNombre.getText().toString();

        if (!TextUtils.isEmpty(nombre)) {
            String claseid = Clases.push().getKey();
            Clases leccion = new Clases(claseid, nombre);
            Clases.child("NombresU").child(claseid).setValue(leccion);

            Toast.makeText(registro.this, "clase registrada", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(registro.this, "debe ingresar el nombre", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {

        //Invocamos al método:


        switch (view.getId()) {

            case R.id.botonRegistrar:
                registrarUsuario();
                break;

            case R.id.botonConf:
                registrarClase();

        }

    }
}

/* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        ButtonGuaradar = (Button)findViewById(R.id.buttonguardar);

        ButtonGuaradar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(registro.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}*/